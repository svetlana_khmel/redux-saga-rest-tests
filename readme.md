### How to Test React Components using Jest and Enzyme ###

- https://blog.bitsrc.io/how-to-test-react-components-using-jest-and-enzyme-fab851a43875

- https://hackernoon.com/unit-testing-redux-connected-components-692fa3c4441c

- https://redux.js.org/recipes/writingtests

- https://redux-saga.js.org/docs/advanced/Testing.html

- https://medium.com/@gethylgeorge/using-redux-saga-to-handle-side-effects-and-testing-it-using-jest-2dff2d59f899

- https://codereviewvideos.com/course/react-redux-and-redux-saga-with-symfony-3/video/testing-javascript-s-fetch-with-jest-happy-path

- https://www.javascriptjanuary.com/blog/may-cause-side-effects-how-to-implement-redux-sagas-as-middleware

- https://blog.benestudio.co/redux-saga-to-the-rescue-607dd3d70d0f

- https://github.com/redux-saga/redux-saga/blob/master/docs/advanced/Testing.md

- https://blog.scottlogic.com/2018/01/16/evaluating-redux-saga-test-libraries.html

- https://github.com/airbnb/enzyme

## Snapshot testing ##
- https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f
- https://jestjs.io/docs/en/snapshot-testing

- Smulate thrown errors with providers: 
- http://redux-saga-test-plan.jeremyfairbank.com/integration-testing/mocking/static-providers.html#throw-errors
- Unhappy path:
-https://codereviewvideos.com/course/react-redux-and-redux-saga-with-symfony-3/video/testing-javascript-s-fetch-with-jest-unhappy-paths


### command not found: jest ###
- Jest is installed, but is likely in your ./node_modules/.bin directory. You can append that to your command ./node_modules/.bin/jest --updateSnapshot. Since you already have jest as a scripts command in your package.json you can also run it with npm test -- --updateSnapshot. npm automatically adds ./node_modules/.bin to your path.


```angular2html
import { throwError } from 'redux-saga-test-plan/providers';

it('succeeds', () => {
  return expectSaga(saga)
    .provide([
      [call(api.fetchUser), { name: 'Jeremy' }],
    ])
    .put({ type: 'DONE', payload: { name: 'Jeremy' } })
    .run();
});

it('fails', () => {
  return expectSaga(saga)
    .provide([
      [call(api.fetchUser), throwError(new Error('whoops'))],
    ])
    .put({ type: 'FAILED' })
    .run();
});
```


## Usefull tools ##
- https://blog.bitsrc.io/5-tools-for-faster-development-in-react-676f134050f2
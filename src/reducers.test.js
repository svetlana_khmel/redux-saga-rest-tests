import reducer from './reducers';
import actions from './constants';

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      { needUpdate: false }
    )
  });

  it('should handle GET_DATA', () => {
    const payload = {
        "bids": [
          {
            "id": "4cf99800-c81e-11e8-8974-037e81ba72ba",
            "carTitle": "we",
            "amount": "12",
            "created": 1538689310080
          }
        ],
        "_id": "5bb683ce4224d42156fa79fa",
        "firstname": "2",
        "lastname": "2",
        "__v": 0
      }

    expect(
      reducer({}, {
        type: actions.GET_DATA,
        payload
      })
    ).toEqual(
      {"data" : payload}
    );
  })
})

import actions from '../constants';
import { getData } from './index';

describe('actions', () => {
  it('Should create an action to load data to application', () => {
    const payload = {
      "bids": [
        {
          "id": "4cf99800-c81e-11e8-8974-037e81ba72ba",
          "carTitle": "we",
          "amount": "12",
          "created": 1538689310080
        }
      ],
      "_id": "5bb683ce4224d42156fa79fa",
      "firstname": "2",
      "lastname": "2",
      "__v": 0
    }

    const expectedAction = {
      type: actions.GET_DATA,
      payload
    };

    expect(getData(payload)).toEqual(expectedAction);
  });
});

import actions from '../constants';

export const loadData = () => ({
  type: actions.LOAD_DATA
});

export const addItem  = () => ({
  type: actions.ADD_ITEM
})

export const editItem = (id) => ({
  type: actions.EDIT_ITEM,
  id
})

export const deleteItem = (id) => ({
  type: actions.DELETE_ITEM,
  id
})

export const getData = payload => ({
  type: actions.GET_DATA,
  payload
});

export const chooseBid = (payload) => ({
  type: actions.CHOOSE_BID,
  payload
});

export const setPageOfItems = (payload) => ({
  type: actions.SET_PAGE_OF_ITEMS,
  payload
});

export const addBidToItem = (id, data) => ({
  type: actions.ADD_BID_TO_ITEM,
  id,
  data
});

export const asyncFetchError = () => ({
  type: actions.FETCH_ERROR
})
import React from 'react';
import MerchantItem from './merchant-item';
import uuidv1 from 'uuid/v1';
import PropTypes from 'prop-types';

const merchantList = ({merchants, changeItem, deleteItem, addBid}) => {
  return (
    <div>
      { merchants ? (merchants.map((merchant) => <MerchantItem key={uuidv1()} changeItem={changeItem} deleteItem={deleteItem} addBid={addBid} merchant={merchant} bids={merchant.bids} />)) : (<div> Loading...</div>)}
    </div>
  )
}
export default merchantList;

merchantList.propTypes = {
  merchants: PropTypes.array,
  changeItem: PropTypes.func,
  deleteItem: PropTypes.func,
  addBid: PropTypes.func
}
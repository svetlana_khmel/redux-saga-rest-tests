import React from 'react';
import Bids from './bit-item';
import uuid1 from 'uuid/v1';
import PropTypes from 'prop-types';

const merchantItem = ({merchant, changeItem, deleteItem, addBid}) => {
  const bids =  [...merchant.bids];
  return (<div className={'merchant-container'}>
    <div className={'edit-merchant'} onClick={() => {changeItem(merchant)}}> EDIT</div>
    <div className={'delete-merchant'} onClick={() => deleteItem(merchant._id)}>{'    merchant._id '+ merchant._id} DELETE</div>
    <div className={'add-bid'} onClick={() => addBid(merchant)}> Add bid</div>
    <div className={'merchant-details-container'}>
      <div>{merchant._id}</div>
      <div>{merchant.firstname}</div>
      <div>{merchant.lastname}</div>
      <div>{merchant.avatarUrl}</div>
      <div>{merchant.email}</div>
      <div>{merchant.phone}</div>
      <div>{merchant.hasPremium}</div>
    </div>
    <div className={'bids-header'}>BIDS:</div>
    {bids ? (bids.sort((a, b) => a.created < b.created).map(bid => <Bids key={uuid1()} bid={bid}/>)) : (<div>Loading...</div>)}
  </div>)
}

export default merchantItem;

merchantItem.propTypes = {
  merchant: PropTypes.object,
  changeItem: PropTypes.func,
  deleteItem: PropTypes.func,
  addBid: PropTypes.func
}


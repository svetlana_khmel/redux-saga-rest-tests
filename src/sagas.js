import { select, put, call, takeLatest, all } from 'redux-saga/effects';
import actions from './constants';
import { loadData, getData, asyncFetchError } from './actions/index';
const baseUrl = 'http://localhost:3300/api/merchants/';
import fetch from 'node-fetch'; //for tests

const headers = {
  'Accept': 'application.json',
  'Content-Type': 'application/json'
}

export function asyncFetch(id, method, data) {
  const body = JSON.stringify(data)

  return fetch(`${baseUrl}${id ? id: ''}`, {
    method,
    headers,
    body
  })
  .then(response => response.json());
}

export function* fetchData() {
  try {
    const payload = yield call(asyncFetch)
    yield put(getData(payload))
  } catch (error) {
    console.log("Error in fetch" + error)
    yield put(asyncFetchError())
  }
}

export function* editItem({id}) {
  const form = yield select();
  const data = form ? form.form.merchantForm.values : {};

  try {
    yield call(asyncFetch, id, 'PUT', data);
    yield put(loadData());
  } catch(error) {
    console.log("Error in fetch" + error)
    yield put(asyncFetchError())
  }
}

export function* addItem() {
  const form = yield select();
  const data = form ? form.form.merchantForm.values : {};

  try {
    yield call(asyncFetch, null, 'POST', data);
    yield put(loadData());
  } catch(error) {
    console.log("Error in fetch" + error)
    yield put(asyncFetchError())
  }
}

export function* deleteItem({id}) {
  try {
    yield call(asyncFetch, id, 'DELETE', data);
    yield put(loadData());
  } catch(error) {
    console.log("Error in fetch" + error)
    yield put(asyncFetchError())
  }
}

export function* addBidToItem({id, data}) {
  try {
    yield call(asyncFetch, id, 'PUT', data);
    yield put(loadData());
  } catch(error) {
    console.log("Error in fetch" + error)
    yield put(asyncFetchError())
  }
}

export function* actionWatcher() {
  yield takeLatest(actions.LOAD_DATA, fetchData);
  yield takeLatest(actions.EDIT_ITEM, editItem);
  yield takeLatest(actions.ADD_ITEM, addItem);
  yield takeLatest(actions.DELETE_ITEM, deleteItem);
  yield takeLatest(actions.ADD_BID_TO_ITEM, addBidToItem);
}

export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}


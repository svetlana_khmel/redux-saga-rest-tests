import actions from './constants';

export default (state = {needUpdate: false}, action) => {
  const id =  action.id;
  switch (action.type) {
    case actions.UPDATE_DATA:
      return {...state, ...action.payload}

    case actions.DELETE_ITEM:
      return {...state, id}

    case actions.EDIT_ITEM:
      console.log('******** EDIT_ITEM ', id);
      return {...state, id}

    case actions.LOAD_DATA:
      console.log('******** LOAD_DATA');
      return {...state, loading: true };

    case actions.GET_DATA:
      console.log('******** GET_DATA', action.payload);
      const data = action.payload;
      return {...state, data}

     case actions.CHOOSE_BID:
      const choosenMerchant = action.payload;
      return {...state, choosenMerchant}

    case actions.SET_PAGE_OF_ITEMS:
      const pageOfItems = action.payload;
      return {...state, pageOfItems}
  }

  return state;
};

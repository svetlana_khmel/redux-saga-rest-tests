import { select, put, call, takeLatest, all } from 'redux-saga/effects';
//const fetchMock = require('fetch-mock');
import { loadData, getData } from './actions/index';

import rootSaga, { fetchData, editItem, addItem, deleteItem, addBidToItem, asyncFetch, actionWatcher } from './sagas';


// describe('Fetch Data', () => {
//
//   const payload = { bids:
//     [ { id: '4cf99800-c81e-11e8-8974-037e81ba72ba',
//       carTitle: 'we',
//       amount: '12',
//       created: 1538689310080 } ],
//     _id: '5bb683ce4224d42156fa79fa',
//     firstname: '2',
//     lastname: '2',
//     __v: 0 }

//
//   const generator = fetchData();
//
//   it('Must call fetch ', () => {
//     const testValue = generator.next().value;
//     expect(testValue).toEqual(call(asyncFetch));
//   });
//
//   it('Must put getData', () => {
//     const dummyOutput = "Dummy Output";
//     const testValue = generator.next().value;
//
//     expect(testValue).toEqual(put(getData()))
//   })
//
// });

describe ('Edit Item', () => {
  const generator = editItem({id: 1});

  it('Should get Select', () => {
    const result = generator.next().value;
    expect(result).toEqual(select());
  })

  it('Should handle call asyncFetch ', () => {
    const testValue = generator.next().value;
    expect(testValue).toEqual(call(asyncFetch, 1, 'PUT', {}))
  })

  it('Should handle put loadData', () => {
    const testValue = generator.next().value;
    expect(testValue).toEqual(put(loadData()))
  })
});

describe ('Add Item', () => {
  const generator = addItem();

  it('Should get Select', () => {
    const result = generator.next().value;
    expect(result).toEqual(select());
  });

  it('Should call asyncFetch', () => {
    const testValue = generator.next().value;
    expect(testValue).toEqual(call(asyncFetch, null, 'POST', {}))
  });

  it('Should handle put loadData', () => {
    const testValue = generator.next().value;
    expect(testValue).toEqual(put(loadData()));
  });
});

// describe('Delete Item', () => {
//   const generator = deleteItem({id: 1});
//
//   it('Should handle call AsyncFetch', () => {
//     const testValue = generator.next().value;
//     expect(testValue).toEqual(call(asyncFetch, 1, 'DELETE', {}));
//   });
//
//   it('Should handle put loadData', () => {
//     const testValue = generator.next().value;
//     expect(testValue).toEqual(put(loadData()));
//   })
// })

describe ('Root Saga ', () => {
  const generator = rootSaga();
  it('calls `all()` with the correct functions', () => {
    expect(generator.next().value).toEqual(all([
      actionWatcher()
    ]))
  })
})


// describe("fetching posts", () => {
//   const generator = cloneableGenerator(fetchPostSaga)();
//   test(‘fetching posts successfully’, () => {
//     const clone = generator.clone();
//     const posts = {};
//
//     expect(clone.next().value).toEqual(call(fetchPostsApi));
//     expect(clone.next(posts).value)
//       .toEqual(put(postsReceived(posts)));
//     expect(clone.next().done).toBe(true);
//   });
// });



import React from 'react';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import App from './App';
import renderer from 'react-test-renderer';

import MerchantList from './components/merchant-list';
import MerchantForm from './containers/merchant-form';
import BidForm from './containers/bid-form';
import Pagination from './components/pagination';

const mockStore = configureMockStore();
const store = mockStore({});

it('App renders correctly', () => {
  const tree = renderer
    .create(<Provider store={store}><App /></Provider>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('MerchantList renders correctly', () => {
  const tree = renderer
    .create(<MerchantList />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('MerchantForm renders correctly', () => {
  const tree = renderer
    .create(<Provider store={store}><MerchantForm /></Provider>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('BidForm renders correctly', () => {
  const tree = renderer
    .create(<Provider store={store}><BidForm /></Provider>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('Pagination renders correctly', () => {
  const tree = renderer
    .create(<Pagination />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from '../components/render-field';
import { required, number } from '../validation';

let BidForm = ({handleSubmit}) => {
    return (
      <div className={'bid-form'}>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="merchantId">Merchant id</label>
            <Field name="merchantId" component="input" type="text" validate={[ required ]}/>
          </div>
          <div>
            <label htmlFor="carTitle">Car title</label>
            <Field name="carTitle"  component={ renderField } type="text" validate={[ required ]} />
          </div>
          <div>
            <label htmlFor="amount">Amount</label>
            <Field name="amount"  component={ renderField } type="text" validate={[ required, number ]} />
          </div>
          <button type="submit">Submit</button>
        </form>
      </div>
    );
}

BidForm = reduxForm({
  form: 'bidForm'
})(BidForm)

export default BidForm
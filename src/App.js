import React, { Component } from 'react';
import { connect } from 'react-redux';
import uuidv1 from 'uuid/v1';
import { loadData, editItem, deleteItem, addItem, setPageOfItems, chooseBid, addBidToItem } from './actions';
import { change } from 'redux-form';
//import './App.css';
//import './Merchant.css';
import MerchantList from './components/merchant-list';
import MerchantForm from './containers/merchant-form';
import BidForm from './containers/bid-form';
import Pagination from './components/pagination';

class App extends Component {
  constructor () {
    super();

    // an example array of items to be paged
    this.state = {
      editMerchant: false,
      id: null,
      exampleItems: []
    }
    this.changeItem = this.changeItem.bind(this);
    this.editMerchantItem = this.editMerchantItem.bind(this);
    this.addBid = this.addBid.bind(this);
    this.addBidToMerchantItem = this.addBidToMerchantItem.bind(this);
    this.getValues = this.getValues.bind(this);

    this.onChangePage = this.onChangePage.bind(this);
  }

  componentDidMount () {
   this.props.loadData();
  }

  componentDidUpdate() {
    if (this.props.merchants.needUpdate) {
      this.props.loadData();
    }
  }

  getValues (data, type) {
    if (type === 'array') {
      return [
        'firstname', data.firstname,
        'lastname', data.lastname,
        'avatarUrl', data.avatarUrl,
        'hasPremium', data.hasPremium,
        'country', data.country,
        'bids', data.bids
      ]
    }

    if (type === 'obj') {
      return {
        'firstname': data.firstname,
        'lastname': data.lastname,
        'avatarUrl': data.avatarUrl,
        'hasPremium': data.hasPremium,
        'country': data.country,
        'bids': data.bids ? data.bids : []
      }
    }
  }

  onChangePage(merchants) {
    this.props.setPageOfItems(merchants);
  }

  changeItem (data) {
    const values = this.getValues(data, 'array');

    values.map((value) => {
      this.props.change(['merchantForm', value, data[value]]);
    });

    this.setState({
      editMerchant: true,
      id: data._id
    })
  }

  editMerchantItem () {
    const { editItem, addItem } = this.props;

    if (this.state.editMerchant) {
      editItem(this.state.id);
    } else {
      addItem();
    }
  }

  addBidToMerchantItem (data) {
    let { choosenMerchant } = this.props.merchants;
    let merchantData = this.getValues(choosenMerchant, 'obj');

    const bidData = {
      id: uuidv1(),
      carTitle: data.carTitle,
      amount: data.amount,
      created: new Date().getTime()
    };

    merchantData.bids.push(bidData);
    this.setState({
      edit: false,
    });
    this.props.addBidToItem(choosenMerchant._id, merchantData);
  }

  addBid (merchant) {
    this.props.change(['bidForm', 'merchantId', merchant._id]);

    this.setState({
      edit: true,
    });

    this.props.chooseBid(merchant);
  }

  render() {
    const { deleteMerchantItem } = this.props;
    const { merchants } = this.props;

    let dataLoaded = false;
    let pageOfItems = [];

    if(merchants && merchants.data && merchants.data.length > 0) {
      dataLoaded = true;
      pageOfItems = merchants.pageOfItems;
    }

    return (
      <div className="App">
        <h1>Test page</h1>
        <div>
          <MerchantForm onSubmit={ this.editMerchantItem } />
          <div className={this.state.edit ? 'show' : 'hide'}>
            <BidForm onSubmit={ this.addBidToMerchantItem } />
          </div>
        </div>
        { dataLoaded &&
          <MerchantList key={uuidv1()} changeItem={ this.changeItem } deleteItem={ deleteMerchantItem } merchants={ pageOfItems } addBid={this.addBid} />
        }
        { dataLoaded &&
          <Pagination items={merchants.data} onChangePage={this.onChangePage} />
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    form: state.form,
    merchants: state.merchants
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadData: () => {
      dispatch(loadData())
    },
    addItem: () => {
      dispatch(addItem())
    },
    editItem: (id) => {
      dispatch(editItem(id))
    },
    deleteMerchantItem: (id) => {
      dispatch(deleteItem(id))
    },
    change: (values) => {
     dispatch(change(...values))
    },
    chooseBid: (data) => {
     dispatch(chooseBid(data))
    },
    addBidToItem: (id, data) => {
     dispatch(addBidToItem(id, data))
    },
    setPageOfItems: (data) => {
      dispatch(setPageOfItems(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);




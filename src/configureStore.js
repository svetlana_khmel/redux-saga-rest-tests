import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga'
import merchants from './reducers';
import mySaga from './sagas'

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  form: reduxFormReducer,
  merchants
})

const configureStore = (preloadedState) => {
  const enhancers = [];

  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.devToolsExtension

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    }
  }

  return createStore(
    reducers,
    preloadedState,
    applyMiddleware(sagaMiddleware),
    compose(
      ...enhancers
    )
  );

  sagaMiddleware.run(mySaga);
};

export default configureStore;



import React from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { reducer as reduxFormReducer } from 'redux-form';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
//import './index.css';
import App from './App';
import merchants from './reducers';
import mySaga from './sagas';
import registerServiceWorker from './registerServiceWorker';

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  form: reduxFormReducer,
  merchants
})

const store = createStore(
  reducers,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(mySaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();

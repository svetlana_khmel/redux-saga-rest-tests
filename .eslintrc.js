module.exports = {
  "plugins": ["jest"],
  "extends": ["airbnb", "plugin:jest/recommended"],
  "rules": {
    "comma-dangle": "error",
    "react/require-default-props": "off",
    "react/jsx-filename-extension": "off", // reason: https://github.com/facebook/create-react-app/issues/87#issuecomment-234627904
    "function-paren-newline": "off",
    "jsx-a11y/media-has-caption": "off"
  },
  "env": {
    "jest/globals": true
  },
  "globals": {
    "shallow": true,
    "mount": true,
    "render": true,
    "window": true,
    "Event": true
  }
};
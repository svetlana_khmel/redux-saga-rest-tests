module.exports = {
  setupTestFrameworkScriptFile: './test-setup.js',
  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95
    }
  }
};
